
const form = document.querySelector('form');
const {ipcRenderer, remote} = require('electron');
const dialog = remote.dialog;
const app = remote.app;
const Papa = require('papaparse');
const fs = require('fs');

const messageBottom = document.getElementById('messageBottom');
const messageTop = document.getElementById('messageTop');
const clearBtn = document.getElementById('clearBtn');
const exportBtn = document.getElementById('exportBtn');
const openFileBtn = document.getElementById('openFileBtn');

// Array containing list of headers used to import inventory
// src: https://retail-support.lightspeedhq.com/hc/en-us/articles/115005142408
const lsHeaders = [
    'Description',
    'UPC',
    'EAN',
    'Custom SKU',
    'Manufacturer SKU',
    'Vendor',
    'Vendor ID',
    'Brand',
    'Default Cost',
    'Default - Price',
    'MSRP - Price',
    'Online - Price',
    'Matrix Description',
    'Matrix Attribute Set',
    'Attribute 1',
    'Attribute 2',
    'Attribute 3',
    'Discountable',
    'Taxable',
    'Tax Class',
    'Item Type',
    'Publish to eCom',
    'Serialized',
    'Category',
    'Subcategory 1',
    'Subcategory 2',
    'Subcategory 3',
    'Clear Existing Tags',
    'Add Tags',
    'Note',
    'Display Note',
    'Archive',
    'Featured Image',
    'Image',
    'Shop Quantity on Hand',
    'Shop Unit Cost',
    'Shop Reorder Point',
    'Shop Reorder Level'
];

// Array of Lightspeed headers that are likely to correspond to a vendor's import file
const dropdownHeaders = [
    'Description',
    'UPC',
    'EAN',
    'Custom SKU',
    'Manufacturer SKU',
    'Vendor',
    'Brand',
    'Default Cost',
    'Default - Price',
    'MSRP - Price',
    'Matrix Description',
    'Matrix Attribute Set',
    'Attribute 1',
    'Attribute 2',
    'Attribute 3',
    'Shop Quantity on Hand',
    'Shop Unit Cost'
];

openFileBtn.addEventListener('click', showOpenFile);

function showOpenFile(e) {
    dialog.showOpenDialog({
        properties: ['openFile'],
        filters: [{name: 'CSV', extensions: ['csv']}]
    }).then(result => {
        console.log(result.filePaths[0]);
        let file = result.filePaths[0];
        ipcRenderer.send('file:open', file);
    });


}

// Catch data from openFile.html
ipcRenderer.on('file:open', function(e, file){

    const content = fs.readFileSync(file, "utf8");

    let headerRow;
    let dataRows;

    // Parse csv file and separate header from rest if data
    Papa.parse(content, {
        complete: results => {
            headerRow = results.data.splice(0, 1)[0];
            dataRows = results.data;
        }
    });

    const table = document.querySelector('#csvHeadersBody');

    // Clear table body before loading file
    table.innerHTML = ""; 

    let dropDown;
    let headers;

    // Build select inputs for edit header form
    let options = '<option value="" selected disabled hidden>Select...</option><option value="remove">--REMOVE--</option>';

    dropdownHeaders.forEach(heading => {
        options += '<option value="' + heading + '">' +
            heading +
            '</option>';
    });


    headerRow.forEach(heading => {
        dropDown = document.createElement('select');
        dropDown.setAttribute('name', heading);
        dropDown.classList.add('form-control');
        dropDown.innerHTML = options;
        dropDown.required = true;

        let div = document.createElement('div');
        div.classList.add('form-group');
        let tr = document.createElement('tr');
        let td = document.createElement('td');
        td.textContent = heading;
        tr.appendChild(td);
        div.appendChild(dropDown)
        tr.appendChild(div);
        table.appendChild(tr);
    });

    // display input file path
    messageTop.innerHTML = 'File imported from: <br>' + file;

    // Show results
    form.classList.remove('hide');

    // Handle submit of edited column headers
    form.addEventListener('submit', submitForm);

    function submitForm(e) {
        e.preventDefault();

        const vendor = document.querySelector('#vendor').value;
        const taxClass = document.querySelector('#taxClass').value;

        let exportHeader = [];
        let exportData = [];
        let removeIndex = [];

        // get all 'select' elements in form
        const selects = [...document.querySelectorAll('select')];
        selects.forEach(select => {
            if (select.id !== 'taxClass') {
                exportHeader.push(select.value);
            }
        });

        // Build array of updated headers
        exportHeader.forEach(function (header, index) {
            if (header === 'remove') {
                removeIndex.push(index);
            }
        });

        // Remove unwanted headers from array
        exportHeader = exportHeader.filter(item => item !== 'remove');

        // Add extra headers
        exportHeader.push('Vendor');
        exportHeader.push('Tax Class');

        // Build array arrays containing row data
        dataRows.forEach(row => {
            for (let i = removeIndex.length -1; i >= 0; i--) row.splice(removeIndex[i],1);
            row.push(vendor);
            row.push(taxClass);
            exportData.push(row);
        });

        // Build array of unused headers and apend to export array
        let unusedHeaders = lsHeaders.filter( el => !exportHeader.includes(el));
        unusedHeaders.forEach( header => {
            exportHeader.push(header);
        });

        // Unparse data back to csv
        const exportCsv = Papa.unparse({
            'fields': exportHeader,
            'data': exportData
        });

        // Export and save modified file
        let filename = dialog.showSaveDialog({
            defaultPath: app.getPath('documents') + '/export.csv',
            title: 'Save file as .csv',
            filters: [
                { name: 'csv', extensions: ['csv'] }
            ]
        }
        ).then(result => {
            filename = result.filePath;
            if (filename === undefined) {
                alert('File not saved');
                return;
            }
            fs.writeFile(filename, exportCsv, (err) => {
                if (err) {
                    alert('An error ocurred with file creation ' + err.message);
                    return
                }
                alert('File saved');
                messageBottom.innerHTML = 'File exported to: <br>' + filename;
                messageBottom.classList.remove('hide');
                exportBtn.disabled = true;
            })
        }).catch(err => {
            console.log(err)
        }) /* end save file */
    };

    clearBtn.addEventListener('click', clearWindow);

    function clearWindow(e) {
        // Reload app
        location.reload();
    }


});


