// Load dependencies
const  {app, BrowserWindow, Menu, dialog, ipcMain} = require('electron');
const url = require('url');
const path = require('path');

// SET ENV
process.env.NODE_ENV = 'production';

let mainWindow;

// Listen for app to be ready
app.on('ready', function() {

    // Create new window
    mainWindow = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true
        },
        title: 'Lightspeed Imports Converter',
        width: 800,
        height: 600
    });

    // Load HMTL into window
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true

    }));

    // Quit on close
    mainWindow.on('closed', function() {
        app.quit();
    });

    // Build menu from template
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);

    // Insert menu
    Menu.setApplicationMenu(mainMenu);

});

// Handle add item window
function createOpenFileWindow(){
    openFileWindow = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true
        },
        width: 600,
        height: 400,
        title: 'Open input file'
    });
    openFileWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'openFile.html'),
        protocol: 'file:',
        slashes:true,
    }));

    // Handle garbage collection
    openFileWindow.on('close', function(){
        openFileWindow = null;
    });

}

// Catch item:add
ipcMain.on('file:open', function(e, file){
    mainWindow.webContents.send('file:open', file);
});

// Create menu template
const mainMenuTemplate = [
    {
        label: 'File',
        submenu: [
            {
                label: 'Quit',
                accelerator: 'CommandOrControl+Q',
                click() {
                    app.quit();
                }
            }
        ]
    },
    {
        label: 'View',
        submenu: [
            { role: 'reload' },
            { type: 'separator' },
            { role: 'resetzoom' },
            { 
                role: 'zoomin',
                accelerator: 'CommandOrControl+=' 
            },
            { 
                role: 'zoomout',
                accelerator: 'CommandOrControl+-'
            },
            { type: 'separator' },
        ]
    },
];

// If MacOS, add empty object to menu
if (process.platform == 'darwin') {
    mainMenuTemplate.unshift({});
};

// Add developer tools item if not in production
if (process.env.NODE_ENV !== 'production') {
    mainMenuTemplate.push({
        label: 'Tools',
        submenu: [
            {
                label: 'Developer tools',
                accelerator:'CommandOrControl+I',
                click(item, focusedWindow) {
                    focusedWindow.toggleDevTools();
                }
            }
        ]
    });
};
